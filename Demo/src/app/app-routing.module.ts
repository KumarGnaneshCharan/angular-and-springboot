import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ShowemployeeComponent } from './showemployee/showemployee.component';
import { ShowEmpByIdComponent } from './show-emp-by-id/show-emp-by-id.component';
import { LogoutComponent } from './logout/logout.component';
import { ProductsComponent } from './products/products.component';
import { authGuard } from './auth.guard';
import { CartComponent } from './cart/cart.component';
import { auth2Guard } from './auth2.guard';

const routes: Routes = [
  {path:"", component:LoginComponent},
  {path:"login", component:LoginComponent},
  {path:"register", component:RegisterComponent},
  {path:"showemps", canActivate:[authGuard], component:ShowemployeeComponent},
  {path:"showempbyid", canActivate:[authGuard], component:ShowEmpByIdComponent},
  {path:"prods", canActivate:[authGuard], component:ProductsComponent},
  {path:"logout", canActivate:[authGuard], component:LogoutComponent},
  {path:"cart", canActivate:[authGuard], component:CartComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
