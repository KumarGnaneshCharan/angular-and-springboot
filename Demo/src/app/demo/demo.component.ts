import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-demo',
  templateUrl: './demo.component.html',
  styleUrls: ['./demo.component.css']
})
export class DemoComponent implements OnInit {
  constructor() { }

  ngOnInit(): void {
    this.setupSmoothScrolling();
  }

  setupSmoothScrolling() {
    document.addEventListener("DOMContentLoaded", () => {
      const links = document.querySelectorAll("a");
      const linkArray = Array.from(links);
      for (const link of linkArray) {
        link.addEventListener("click", this.smoothScroll.bind(this));
      }
    });
  }

  smoothScroll($event: any) {
    $event.preventDefault();
    const targetId = $event.target.getAttribute("href").substring(1);
    const targetSection = document.getElementById(targetId);
    if (targetSection) {
      window.scrollTo({
        top: targetSection.offsetTop,
        behavior: "smooth",
      });
    }
  }

}
