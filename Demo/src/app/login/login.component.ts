import { Component, OnInit} from '@angular/core';
import { EmpService } from '../emp.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  emailId:any;
  password:any;
  employees:any;
  status:any;
  emp:any;

  constructor(private service: EmpService, private router:Router){
    // this.employees=[
    //   {
    //     empId: 1,
    //     empName: "John Smith",
    //     salary: 60000,
    //     gender: "Male",
    //     doj: "2023-01-15",
    //     country: "USA",
    //     emailId: "john.smith@example.com",
    //     password: "P@ssw0rd1"
    //   },
    //   {
    //     empId: 2,
    //     empName: "Jane Doe",
    //     salary: 55000,
    //     gender: "Female",
    //     doj: "2022-11-10",
    //     country: "Canada",
    //     emailId: "jane.doe@example.com",
    //     password: "P@ssw0rd2"
    //   },
    //   {
    //     empId: 3,
    //     empName: "Michael Johnson",
    //     salary: 65000,
    //     gender: "Male",
    //     doj: "2023-03-20",
    //     country: "USA",
    //     emailId: "michael.johnson@example.com",
    //     password: "P@ssw0rd3"
    //   },
    //   {
    //     empId: 4,
    //     empName: "Emily Davis",
    //     salary: 58000,
    //     gender: "Female",
    //     doj: "2022-09-05",
    //     country: "Canada",
    //     emailId: "emily.davis@example.com",
    //     password: "P@ssw0rd4"
    //   },
    //   {
    //     empId: 5,
    //     empName: "David Wilson",
    //     salary: 62000,
    //     gender: "Male",
    //     doj: "2023-02-25",
    //     country: "USA",
    //     emailId: "david.wilson@example.com",
    //     password: "P@ssw0rd5"
    //   }
    // ]
  }

  ngOnInit(){ 
  }

  async loginSubmit(loginForm : any){
    localStorage.setItem("emailId", loginForm.emailId);
    if(loginForm.emailId=="HR" && loginForm.password=="HR"){
      this.service.setUserLogin();
      this.router.navigate(['prods']);
    }
    else{
      this.emp=null;
      await this.service.empLogin(loginForm).then((empData:any)=>{
        this.emp=empData;
      });
      if (this.emp != null) {
        this.service.setUserLogin();
        this.router.navigate(['prods']);
      } else {
        alert("Invalid Credentials");
      }
    }
  }
}
