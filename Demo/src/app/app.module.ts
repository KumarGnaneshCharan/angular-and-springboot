import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TestComponent } from './test/test.component';
import { Test2Component } from './test2/test2.component';
import { LoginComponent } from './login/login.component';
import { FormsModule } from '@angular/forms';
import { RegisterComponent } from './register/register.component';
import { ShowemployeeComponent } from './showemployee/showemployee.component';
import { ShowEmpByIdComponent } from './show-emp-by-id/show-emp-by-id.component';
import { ExpPipePipe } from './exp-pipe.pipe';
import { GenderPipePipe } from './gender-pipe.pipe';
import { HeaderComponent } from './header/header.component';
import { LogoutComponent } from './logout/logout.component';
import { RouterModule } from '@angular/router';
import { ProductsComponent } from './products/products.component';

import { HttpClientModule } from '@angular/common/http';
import { CartComponent } from './cart/cart.component';
import { DemoComponent } from './demo/demo.component';
import { CaptchaComponent } from './captcha/captcha.component';
import { RecaptchaComponent } from './recaptcha/recaptcha.component';
import { ReCaptchaV3Service } from 'ngx-captcha';


@NgModule({
  declarations: [
    AppComponent,
    TestComponent,
    Test2Component,
    LoginComponent,
    RegisterComponent,
    ShowemployeeComponent,
    ShowEmpByIdComponent,
    ExpPipePipe,
    GenderPipePipe,
    HeaderComponent,
    LogoutComponent,
    ProductsComponent,
    CartComponent,
    DemoComponent,
    CaptchaComponent,
    RecaptchaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,   //two way data binding
    RouterModule,  //for routing through TS
    HttpClientModule, //for HTTP client, for accessing ecternal apis
    ReCaptchaV3Service
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
