import { Component, OnInit, numberAttribute } from '@angular/core';
import { EmpService } from '../emp.service';

@Component({
  selector: 'app-show-emp-by-id',
  templateUrl: './show-emp-by-id.component.html',
  styleUrls: ['./show-emp-by-id.component.css']
})
export class ShowEmpByIdComponent implements OnInit {
  // employees:any;
  empId:any;
  employee:any;
  emailId:any;
  constructor(private service:EmpService){
    this.emailId=localStorage.getItem("emailId");
    // this.employees=[
    //   {
    //     empId: 1,
    //     empName: "John Smith",
    //     salary: 60000,
    //     gender: "Male",
    //     doj: "2023-01-15",
    //     country: "USA",
    //     emailId: "john.smith@example.com",
    //     password: "P@ssw0rd1"
    //   },
    //   {
    //     empId: 2,
    //     empName: "Jane Doe",
    //     salary: 55000,
    //     gender: "Female",
    //     doj: "2022-11-10",
    //     country: "Canada",
    //     emailId: "jane.doe@example.com",
    //     password: "P@ssw0rd2"
    //   },
    //   {
    //     empId: 3,
    //     empName: "Michael Johnson",
    //     salary: 65000,
    //     gender: "Male",
    //     doj: "2023-03-20",
    //     country: "USA",
    //     emailId: "michael.johnson@example.com",
    //     password: "P@ssw0rd3"
    //   },
    //   {
    //     empId: 4,
    //     empName: "Emily Davis",
    //     salary: 58000,
    //     gender: "Female",
    //     doj: "2022-09-05",
    //     country: "Canada",
    //     emailId: "emily.davis@example.com",
    //     password: "P@ssw0rd4"
    //   },
    //   {
    //     empId: 5,
    //     empName: "David Wilson",
    //     salary: 62000,
    //     gender: "Male",
    //     doj: "2023-02-25",
    //     country: "USA",
    //     emailId: "david.wilson@example.com",
    //     password: "P@ssw0rd5"
    //   }
    // ]
  }
  ngOnInit(){

  }
  getEmp(){
    this.service.getEmployeeById(this.empId).subscribe((data:any)=>{
      this.employee=data;
    });
  }
}
