import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject,Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmpService {

  loginStatus:Subject<any>;
  isUserLogged:boolean;
  cartItem:any;

  constructor(private http:HttpClient) { 
    this.isUserLogged=false;
    this.loginStatus=new Subject();
    this.cartItem=[];
  }

  getCountries(): any{
    return this.http.get('https://restcountries.com/v3.1/all');
  }

  getAllEmployees(): any {
    return this.http.get('getAllEmployees');
  }

  getEmployeeById(empId:any):any{
    return this.http.get('getEmpById/'+empId);
  }

  getAllDepartments():any{
    return this.http.get('getAllDepartments');
  }

  registerEmployee(emp:any):any{
    return this.http.post('registerEmployee',emp);
  }

  updateEmployee(emp:any):any{
    return this.http.put('updateEmployee',emp);
  }

  deleteEmployee(empId:any):any{
    return this.http.delete('deleteEmpById/'+empId);
  }

  empLogin(loginForm: any) {
    return this.http.get("empLogin/" + loginForm.emailId + "/" + loginForm.password).toPromise();
  }

  addToCart(prod:any){
    this.cartItem.push(prod);
  }

  // verifyCaptcha(headers: HttpHeaders, response: string): Observable<any> {
  //   const requestBody = { recaptchaResponse: response };
  //   return this.http.post("verify-captcha", requestBody, { headers });
  // }

  //login
  setUserLogin(){
    this.isUserLogged=true;
  }
  //logout
  setUserLogout(){
    this.isUserLogged=false;
  }
  getIsUserLogged():boolean{
    return  this.isUserLogged ;
  }

  getStatusLogin(){
    return this.loginStatus.asObservable();
  }
}
