import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';
import { HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit{
  employee:any;
  countries:any;
  departments:any;
  siteKey = '6Le8GfYoAAAAAEwCUTnODozbyFWQVPFGkWZQed1S';
  captchaResponse: any;
  captchaService: any;


  constructor(private service:EmpService){
    this.employee = {
      empId:"",
      empName:"",
      salary:"",
      gender:"",
      doj:"",
      country:"",
      emailId:"",
      password:"",
      department:{
        deptId:""
      }
    }
  }

  ngOnInit(){
    this.service.getCountries().subscribe((data:any)=>{
      this.countries= data;
    });

    this.service.getAllDepartments().subscribe((data:any)=>{
      this.departments=data;
    });
  }

  registerSubmit(registerform:any){
    this.service.registerEmployee(this.employee).subscribe((data: any) => {
      console.log(data);
    });

    console.log(this.employee);
    console.log(registerform);
  }

  handleCaptchaResolved(response: string){
    this.captchaResponse = response;

    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    const requestBody = { recaptchaResponse: response };
    this.captchaService.verifyCaptcha(headers, response).subscribe(
      (data: any) => {
        console.log('CAPTCHA verification successful:', data);
        // You can perform further actions upon successful verification
      },
      (error: any) => {
        console.error('CAPTCHA verification failed:', error);
        // Handle the error or display a message to the user
      }
    );


  }
}
