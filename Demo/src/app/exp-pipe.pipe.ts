import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'expPipe'
})
export class ExpPipePipe implements PipeTransform {
  currentYear:any;
  joiningYear:any;
  exp:any;
  transform(value: any,): any {
    this.currentYear = new Date().getFullYear();
    this.joiningYear = new Date(value).getFullYear();
    this.exp=this.currentYear-this.joiningYear;
    return this.exp;
  }

}
