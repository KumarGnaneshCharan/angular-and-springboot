import { Component, OnInit } from '@angular/core';
import { EmpService } from '../emp.service';

declare var jQuery:any;

@Component({
  selector: 'app-showemployee',
  templateUrl: './showemployee.component.html',
  styleUrls: ['./showemployee.component.css']
})
export class ShowemployeeComponent implements OnInit {
  employees:any;
  employee:any;
  departments:any;
  countries:any;

  constructor(private service:EmpService){
    this.employee = {
      empId:"",
      empName:"",
      salary:"",
      gender:"",
      doj:"",
      country:"",
      emailId:"",
      password:"",
      department:{
        deptId:""
      }
    }
  }
  ngOnInit(){
    this.service.getAllEmployees().subscribe((data: any) => {
      this.employees = data;
    });

    this.service.getCountries().subscribe((data:any)=>{
      this.countries= data;
    });

    this.service.getAllDepartments().subscribe((data:any)=>{
      this.departments=data;
    });
  }

  editEmp(emp:any){
    console.log(emp);
    jQuery('#empModel').modal('show');
  }

  updateEmp() {
    this.service.updateEmployee(this.employee).subscribe((data:any)=>{
      console.log(data);
    });
  }

  deleteEmp(emp:any){
    this.service.deleteEmployee(emp.empId).subscribe((data:any)=>{
      console.log(data);
    });
    const i=this.employees.findIndex((employee:any)=>{
      return employee.empId===emp.empId;
    });
    this.employees.splice(i,1);
  }
}



// this.employee=[
    //   {
    //     empId: 1,
    //     empName: "John Smith",
    //     salary: 60000,
    //     gender: "Male",
    //     doj: "2023-01-15",
    //     country: "USA",
    //     emailId: "john.smith@example.com",
    //     password: "P@ssw0rd1"
    //   },
    //   {
    //     empId: 2,
    //     empName: "Jane Doe",
    //     salary: 55000,
    //     gender: "Female",
    //     doj: "2022-11-10",
    //     country: "Canada",
    //     emailId: "jane.doe@example.com",
    //     password: "P@ssw0rd2"
    //   },
    //   {
    //     empId: 3,
    //     empName: "Michael Johnson",
    //     salary: 65000,
    //     gender: "Male",
    //     doj: "2023-03-20",
    //     country: "USA",
    //     emailId: "michael.johnson@example.com",
    //     password: "P@ssw0rd3"
    //   },
    //   {
    //     empId: 4,
    //     empName: "Emily Davis",
    //     salary: 58000,
    //     gender: "Female",
    //     doj: "2022-09-05",
    //     country: "Canada",
    //     emailId: "emily.davis@example.com",
    //     password: "P@ssw0rd4"
    //   },
    //   {
    //     empId: 5,
    //     empName: "David Wilson",
    //     salary: 62000,
    //     gender: "Male",
    //     doj: "2023-02-25",
    //     country: "USA",
    //     emailId: "david.wilson@example.com",
    //     password: "P@ssw0rd5"
    //   }
    // ]